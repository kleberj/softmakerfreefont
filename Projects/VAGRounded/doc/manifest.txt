This work consists of the following files:

-- Styles --

vagrounded.sty


-- map file --

vagrounded.map


-- fd files --

8r6vr.fd
ot16vr.fd
t16vr.fd
ts16vr.fd


-- tfm files --

6vrr7t.tfm
6vrr8a.tfm
6vrr8c.tfm
6vrr8r.tfm
6vrr8t.tfm
6vrrc7t.tfm
6vrrc8t.tfm
6vrro7t.tfm
6vrro8c.tfm
6vrro8r.tfm
6vrro8t.tfm


-- vf files --

6vrr7t.vf
6vrr8c.vf
6vrr8t.vf
6vrrc7t.vf
6vrrc8t.vf
6vrro7t.vf
6vrro8c.vf
6vrro8t.vf


-- Documentation --

vagrounded.pdf
vagrounded.tex


-- Misc. Files --

README
manifest.txt
INSTALL
CHANGES
