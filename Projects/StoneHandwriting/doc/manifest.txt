This work consists of the following files:

-- Styles --

stonehand.sty


-- map file --

stonehand.map


-- fd files --

8r6sh.fd
ot16sh.fd
t16sh.fd
ts16sh.fd


-- tfm files --

6shr7t.tfm
6shr8a.tfm
6shr8c.tfm
6shr8r.tfm
6shr8t.tfm
6shrc7t.tfm
6shrc8t.tfm
6shro7t.tfm
6shro8c.tfm
6shro8r.tfm
6shro8t.tfm


-- vf files --

6shr7t.vf
6shr8c.vf
6shr8t.vf
6shrc7t.vf
6shrc8t.vf
6shro7t.vf
6shro8c.vf
6shro8t.vf


-- Documentation --

stonehand.pdf
stonehand.tex


-- Misc. Files --

README
manifest.txt
INSTALL
CHANGES
