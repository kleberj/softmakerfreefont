This work consists of the following files:

-- Styles --

worcester.sty


-- map file --

worcester.map


-- fd files --

8r6wc.fd
ot16wc.fd
t16wc.fd
ts16wc.fd


-- tfm files --

6wcb7t.tfm
6wcb8a.tfm
6wcb8c.tfm
6wcb8r.tfm
6wcb8t.tfm
6wcbc7t.tfm
6wcbc8t.tfm
6wcbi7t.tfm
6wcbi8a.tfm
6wcbi8c.tfm
6wcbi8r.tfm
6wcbi8t.tfm
6wcbo7t.tfm
6wcbo8c.tfm
6wcbo8r.tfm
6wcbo8t.tfm
6wcr7t.tfm
6wcr8a.tfm
6wcr8c.tfm
6wcr8r.tfm
6wcr8t.tfm
6wcrc7t.tfm
6wcrc8t.tfm
6wcri7t.tfm
6wcri8a.tfm
6wcri8c.tfm
6wcri8r.tfm
6wcri8t.tfm
6wcro7t.tfm
6wcro8c.tfm
6wcro8r.tfm
6wcro8t.tfm


-- vf files --

6wcb7t.vf
6wcb8c.vf
6wcb8t.vf
6wcbc7t.vf
6wcbc8t.vf
6wcbi7t.vf
6wcbi8c.vf
6wcbi8t.vf
6wcbo7t.vf
6wcbo8c.vf
6wcbo8t.vf
6wcr7t.vf
6wcr8c.vf
6wcr8t.vf
6wcrc7t.vf
6wcrc8t.vf
6wcri7t.vf
6wcri8c.vf
6wcri8t.vf
6wcro7t.vf
6wcro8c.vf
6wcro8t.vf


-- Documentation --

worcester.pdf


-- Misc. Files --

README
manifest.txt
INSTALL
CHANGES
