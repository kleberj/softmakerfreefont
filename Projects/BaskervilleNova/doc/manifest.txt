This work consists of the following files:

-- Styles --

baskervillenova.sty


-- map file --

baskervillenova.map


-- fd files --

8r6bn.fd
ot16bn.fd
t16bn.fd
ts16bn.fd


-- tfm files --

6bnb7t.tfm
6bnb8a.tfm
6bnb8c.tfm
6bnb8r.tfm
6bnb8t.tfm
6bnbc7t.tfm
6bnbc8t.tfm
6bnbi7t.tfm
6bnbi8a.tfm
6bnbi8c.tfm
6bnbi8r.tfm
6bnbi8t.tfm
6bnbo7t.tfm
6bnbo8c.tfm
6bnbo8r.tfm
6bnbo8t.tfm
6bnr7t.tfm
6bnr8a.tfm
6bnr8c.tfm
6bnr8r.tfm
6bnr8t.tfm
6bnrc7t.tfm
6bnrc8t.tfm
6bnri7t.tfm
6bnri8a.tfm
6bnri8c.tfm
6bnri8r.tfm
6bnri8t.tfm
6bnro7t.tfm
6bnro8c.tfm
6bnro8r.tfm
6bnro8t.tfm


-- vf files --

6bnb7t.vf
6bnb8c.vf
6bnb8t.vf
6bnbc7t.vf
6bnbc8t.vf
6bnbi7t.vf
6bnbi8c.vf
6bnbi8t.vf
6bnbo7t.vf
6bnbo8c.vf
6bnbo8t.vf
6bnr7t.vf
6bnr8c.vf
6bnr8t.vf
6bnrc7t.vf
6bnrc8t.vf
6bnri7t.vf
6bnri8c.vf
6bnri8t.vf
6bnro7t.vf
6bnro8c.vf
6bnro8t.vf


-- Documentation --

baskervillenova.pdf
baskervillenova.tex


-- Misc. Files --

README
manifest.txt
INSTALL
CHANGES
