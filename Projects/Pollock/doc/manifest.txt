This work consists of the following files:

-- Styles --

pollock.sty


-- map file --

pollock.map


-- fd files --

8r6pl.fd
ot16pl.fd
t16pl.fd
ts16pl.fd


-- tfm files --

6pll7t.tfm
6pll8a.tfm
6pll8c.tfm
6pll8r.tfm
6pll8t.tfm
6pllc7t.tfm
6pllc8t.tfm
6pllo7t.tfm
6pllo8c.tfm
6pllo8r.tfm
6pllo8t.tfm
6plr7t.tfm
6plr8a.tfm
6plr8c.tfm
6plr8r.tfm
6plr8t.tfm
6plrc7t.tfm
6plrc8t.tfm
6plro7t.tfm
6plro8c.tfm
6plro8r.tfm
6plro8t.tfm


-- vf files --

6pll7t.vf
6pll8c.vf
6pll8t.vf
6pllc7t.vf
6pllc8t.vf
6pllo7t.vf
6pllo8c.vf
6pllo8t.vf
6plr7t.vf
6plr8c.vf
6plr8t.vf
6plrc7t.vf
6plrc8t.vf
6plro7t.vf
6plro8c.vf
6plro8t.vf


-- Documentation --

pollock.pdf


-- Misc. Files --

README
manifest.txt
INSTALL
CHANGES
